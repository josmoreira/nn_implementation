from Layer import *
import numpy as np
class L2_Reg(Layer):
    def __init__(self,coef):
        self.coef = coef
        super(L2_Reg,self).__init__("l2_reg")
    def __call__(self,model):
        self.model = model
        return self
    ##bw function does nothing
    def fw(self):
        counter = 0.
        for n in self.model.node_list:
            if hasattr(n,"w"):
                counter += np.sum(np.multiply(n.w,n.w))
        self.fw_result = 0.5*self.coef*counter
    def update(self,n):
        if hasattr(n,"w"):
                n.w -= self.coef * n.w
            
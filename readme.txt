Conv2D layer:
	im2col & col2 img: 8h
	fw pass: 6h
	bw pass: 6h
FC layer:
	fw pass: 4h
	bw pass: 3h

Loss layers:
	MSE layer:
		fw pass: 2h
		bw pass: 2h
	Cross Entropy layer:
		fw pass: 2h
		bw pass: 1h
Input layer: 1h

Max Pool: 4h
	fw pass: 1.5h
	bw pass: 2.5h

Model structure: 2h



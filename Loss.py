import numpy as np
from Layer import *

"""Softmax function"""
def softmax(X):
    eX = np.exp((X.T - np.max(X, axis=1)).T)
    return (eX.T / eX.sum(axis=1)).T


class Loss(Layer):
    """Base class for a loss layer"""
    def __call__(self,prev,input_layer,reg=None):
        self.input_layer = input_layer
        self.reg = reg
        return super(Loss,self).__call__(prev)

    def fw(self):
        if self.reg is not None:
            self.reg.fw()


class MSELoss(Loss):
    """ Mean squared error loss class"""
    def __init__(self):
        super(MSELoss,self).__init__("MSELoss")
    def fw(self):
        X = self.prev[0].fw_result
        if self.batch_size is None:
            self.batch_size = X.shape[0]
        sample_losses = np.square(self.input_layer.target - self.prev[0].fw_result)
        self.fw_result = np.repeat(np.reshape(np.mean(sample_losses,axis=0),((1,)+sample_losses.shape[1:])),self.batch_size,axis=0)
        super(MSELoss,self).fw()
        self.fw_result_reg = self.fw_result + 0 if self.reg is None else self.reg.fw_result

    def bw(self):
        sample_ds = self.prev[0].fw_result - self.input_layer.target
        self.bw_result = sample_ds
        
class CELoss(Loss):
    """Cross Entropy loss layer"""
    def __init__(self):
        super(CELoss,self).__init__("CELoss")
    def fw(self):
        y_pred = self.prev[0].fw_result
        n_pred = y_pred.shape[0]
        y_train = self.input_layer.target.astype(int)
        prob = softmax(y_pred)
        self.fw_result = -np.log(prob[range(n_pred), y_train])
        super(CELoss,self).fw()
        self.fw_result_reg = self.fw_result + 0 if self.reg is None else self.reg.fw_result
        
    def bw(self):
        y_pred = self.prev[0].fw_result
        n_pred = y_pred.shape[0]
        y_train = self.input_layer.target.astype(int)
        self.bw_result = softmax(y_pred)
        self.bw_result[range(n_pred), y_train] -= 1.
        self.bw_result /= n_pred
        #average across batch and repeat avg this should probably be elsewhere, not inside layer itself, maybe on solver...
        #self.bw_result = np.repeat(self.bw_result.mean(axis=0).reshape(1,-1),n_pred,axis=0)